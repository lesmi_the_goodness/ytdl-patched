# coding: utf-8
# AUTOMATICALLY GENERATED FILE. DO NOT EDIT.
# Generated by ./devscripts/make_peertube_instance_list.py
from __future__ import unicode_literals

instances = {
    # list of instances here
    "0ch.tv",
    "22x22.ru",
    "810video.com",
    "all.electric.kitchen",
    "alterscope.fr",
    "anarchy.tube",
    "andyrush.fedihost.io",
    "apathy.tv",
    "aperi.tube",
    "apertatube.net",
    "apollo.lanofthedead.xyz",
    "archive.nocopyrightintended.tv",
    "archive.reclaim.tv",
    "artitube.artifaille.fr",
    "astrotube-ufe.obspm.fr",
    "astrotube.obspm.fr",
    "audio.freediverse.com",
    "avantwhatever.xyz",
    "ballhaus.media",
    "bark.video",
    "battlepenguin.video",
    "bava.tv",
    "beartrix-peertube-u29672.vm.elestio.app",
    "bee-tube.fr",
    "beta.flimmerstunde.xyz",
    "biblion.refchat.net",
    "biblioteca.theowlclub.net",
    "bideoak.argia.eus",
    "bideoak.zeorri.eus",
    "bideoteka.eus",
    "bimbo.video",
    "bitcointv.com",
    "bitforged.stream",
    "blurt.media",
    "bolha.tube",
    "bonn.video",
    "breeze.tube",
    "bridport.tv",
    "brioco.live",
    "brocosoup.fr",
    "canal.facil.services",
    "canard.tube",
    "cast.garden",
    "cdn01.tilvids.com",
    "cfnumerique.tv",
    "christian.freediverse.com",
    "christube.malyon.co.uk",
    "christuncensored.com",
    "christunscripted.com",
    "classe.iro.umontreal.ca",
    "clip.place",
    "cloudtube.ise.fraunhofer.de",
    "coldfishvideo.ddns.net",
    "comics.peertube.biz",
    "commons.tube",
    "communitymedia.video",
    "conf.tube",
    "content.haacksnetworking.org",
    "crank.recoil.org",
    "cuddly.tube",
    "cumraci.tv",
    "dalek.zone",
    "dalliance.network",
    "dangly.parts",
    "darkvapor.nohost.me",
    "davbot.media",
    "den.wtf",
    "digitalcourage.video",
    "diode.zone",
    "displayeurope.video",
    "docker.videos.lecygnenoir.info",
    "dreiecksnebel.alex-detsch.de",
    "dud-video.inf.tu-dresden.de",
    "dud175.inf.tu-dresden.de",
    "dytube.com",
    "earthclimate.tv",
    "ebildungslabor.video",
    "eggflix.foolbazar.eu",
    "evangelisch.video",
    "evilfactorylabs.social",
    "exode.me",
    "exquisite.tube",
    "faf.watch",
    "fair.tube",
    "fedi.video",
    "fedifilm.com",
    "fedimovie.com",
    "feditubo.labaudric.net",
    "feditubo.yt",
    "fediverse.tv",
    "fgage.com",
    "fightforinfo.com",
    "film.fjerland.no",
    "film.k-prod.fr",
    "film.node9.org",
    "flappingcrane.com",
    "flim.txmn.tk",
    "flipboard.video",
    "flooftube.net",
    "fontube.fr",
    "foss.video",
    "fotogramas.politicaconciencia.org",
    "foubtube.com",
    "framatube.org",
    "freediverse.com",
    "freedomtv.pro",
    "freesoto-u2151.vm.elestio.app",
    "freesoto.tv",
    "friprogramvarusyndikatet.tv",
    "garr.tv",
    "gnulinux.tube",
    "go3.site",
    "greatview.video",
    "grypstube.uni-greifswald.de",
    "gultsch.video",
    "hitchtube.fr",
    "hpstube.fr",
    "hyperreal.tube",
    "ibbwstream.schule-bw.de",
    "indymotion.fr",
    "infothema.net",
    "intelligentia.tv",
    "intratube-u25541.vm.elestio.app",
    "irrsinn.video",
    "itvplus.iiens.net",
    "jetstream.watch",
    "johnydeep.net",
    "k-pop.22x22.ru",
    "kadras.live",
    "kamtube.ru",
    "kino.kompot.si",
    "kino.schuerz.at",
    "kinowolnosc.pl",
    "kirche.peertube-host.de",
    "kiwi.froggirl.club",
    "kodcast.com",
    "kolektiva.media",
    "koreus.tv",
    "kpop.22x22.ru",
    "kviz.leemoon.network",
    "lastbreach.tv",
    "leffler.video",
    "librepoop.de",
    "linhtran.eu",
    "literatube.com",
    "live.codinglab.ch",
    "live.dcnh.cloud",
    "live.libratoi.org",
    "live.nanao.moe",
    "live.oldskool.fi",
    "live.solari.com",
    "llamachile.tube",
    "lone.earth",
    "lostpod.space",
    "lucarne.balsamine.be",
    "m.bbbdn.jp",
    "makertube.net",
    "media.assassinate-you.net",
    "media.cooleysekula.net",
    "media.curious.bio",
    "media.exo.cat",
    "media.fsfe.org",
    "media.geekwisdom.org",
    "media.gzevd.de",
    "media.inno3.eu",
    "media.interior.edu.uy",
    "media.krashboyz.org",
    "media.mwit.ac.th",
    "media.mzhd.de",
    "media.nolog.cz",
    "media.notfunk.radio",
    "media.over-world.org",
    "media.sayafe.org",
    "media.smz-ma.de",
    "media.undeadnetwork.de",
    "media.zat.im",
    "medias.debrouillonet.org",
    "medias.pingbase.net",
    "mediathek.rzgierskopp.de",
    "megatube.lilomoino.fr",
    "merci-la-police.fr",
    "meshtube.net",
    "mevideo.host",
    "misnina.tv",
    "mix.video",
    "mla.moe",
    "mountaintown.video",
    "mplayer.demouliere.eu",
    "music.facb69.tec.br",
    "my-sunshine.video",
    "mystic.video",
    "mytube.cooltux.net",
    "mytube.kn-cloud.de",
    "mytube.madzel.de",
    "nadajemy.com",
    "nanawel-peertube.dyndns.org",
    "neat.tube",
    "nekopunktube.fr",
    "neon.cybre.stream",
    "neshweb.tv",
    "nethack.tv",
    "nightshift.minnix.dev",
    "nolog.media",
    "notretube.asselma.eu",
    "ocfedtest.hosted.spacebear.ee",
    "ohayo.rally.guide",
    "openmedia.edunova.it",
    "outcast.am",
    "p.eertu.be",
    "p.efg-ober-ramstadt.de",
    "p.lu",
    "p.nintendojo.fr",
    "p2ptv.ru",
    "pastafriday.club",
    "pbvideo.ru",
    "peer.acidfog.com",
    "peer.azurs.fr",
    "peer.madiator.cloud",
    "peer.philoxweb.be",
    "peer.raise-uav.com",
    "peer.tube",
    "peertube-ardlead-u29325.vm.elestio.app",
    "peertube-docker.cpy.re",
    "peertube-ecogather-u20874.vm.elestio.app",
    "peertube-eu.howlround.com",
    "peertube-ktgou-u11537.vm.elestio.app",
    "peertube-us.howlround.com",
    "peertube-wb4xz-u27447.vm.elestio.app",
    "peertube-will.stay-silly.org",
    "peertube.020.pl",
    "peertube.1312.media",
    "peertube.1984.cz",
    "peertube.2i2l.net",
    "peertube.adresse.data.gouv.fr",
    "peertube.aegrel.ee",
    "peertube.alpharius.io",
    "peertube.am-networks.fr",
    "peertube.amicale.net",
    "peertube.anasodon.com",
    "peertube.anduin.net",
    "peertube.anija.mooo.com",
    "peertube.anon-kenkai.com",
    "peertube.anti-logic.com",
    "peertube.anzui.dev",
    "peertube.apcraft.jp",
    "peertube.apse-asso.fr",
    "peertube.arch-linux.cz",
    "peertube.arnhome.ovh",
    "peertube.art3mis.de",
    "peertube.artica.center",
    "peertube.askan.info",
    "peertube.asp.krakow.pl",
    "peertube.astral0pitek.synology.me",
    "peertube.astrolabe.coop",
    "peertube.atilla.org",
    "peertube.atsuchan.page",
    "peertube.aukfood.net",
    "peertube.automat.click",
    "peertube.axiom-paca.g1.lu",
    "peertube.b38.rural-it.org",
    "peertube.be",
    "peertube.becycle.com",
    "peertube.beeldengeluid.nl",
    "peertube.behostings.net",
    "peertube.bgzashtita.es",
    "peertube.bilange.ca",
    "peertube.bildung-ekhn.de",
    "peertube.bingo-ev.de",
    "peertube.blablalinux.be",
    "peertube.boc47.org",
    "peertube.br0.fr",
    "peertube.bridaahost.ynh.fr",
    "peertube.bubbletea.dev",
    "peertube.bubuit.net",
    "peertube.casually.cat",
    "peertube.ch",
    "peertube.chartilacorp.ru",
    "peertube.chaunchy.com",
    "peertube.chir.rs",
    "peertube.christianpacaud.com",
    "peertube.chtisurel.net",
    "peertube.chuggybumba.com",
    "peertube.cipherbliss.com",
    "peertube.cirkau.art",
    "peertube.cloud.nerdraum.de",
    "peertube.cloud.sans.pub",
    "peertube.cluster.wtf",
    "peertube.communecter.org",
    "peertube.cpy.re",
    "peertube.craftum.pl",
    "peertube.crazy-to-bike.de",
    "peertube.ctseuro.com",
    "peertube.cuatrolibertades.org",
    "peertube.cube4fun.net",
    "peertube.dair-institute.org",
    "peertube.datagueule.tv",
    "peertube.dc.pini.fr",
    "peertube.debian.social",
    "peertube.demonix.fr",
    "peertube.designersethiques.org",
    "peertube.desmu.fr",
    "peertube.devol.it",
    "peertube.dixvaha.com",
    "peertube.dk",
    "peertube.doesstuff.social",
    "peertube.dsmouse.net",
    "peertube.dtth.ch",
    "peertube.dubwise.dk",
    "peertube.dynlinux.io",
    "peertube.eb8.org",
    "peertube.ecologie.bzh",
    "peertube.education-forum.com",
    "peertube.elforcer.ru",
    "peertube.eqver.se",
    "peertube.ethibox.fr",
    "peertube.eticadigital.eu",
    "peertube.eu.org",
    "peertube.european-pirates.eu",
    "peertube.eus",
    "peertube.euskarabildua.eus",
    "peertube.everypizza.im",
    "peertube.existiert.ch",
    "peertube.expi.studio",
    "peertube.f-si.org",
    "peertube.familie-berner.de",
    "peertube.familleboisteau.fr",
    "peertube.fedi.zutto.fi",
    "peertube.fedihost.website",
    "peertube.fedihub.online",
    "peertube.fediversity.eu",
    "peertube.fenarinarsa.com",
    "peertube.festnoz.de",
    "peertube.flauschbereich.de",
    "peertube.florentcurk.com",
    "peertube.forteza.fr",
    "peertube.fr",
    "peertube.funkfeuer.at",
    "peertube.futo.org",
    "peertube.g2od.ch",
    "peertube.gaialabs.ch",
    "peertube.gargantia.fr",
    "peertube.geekgalaxy.fr",
    "peertube.gegeweb.eu",
    "peertube.gemlog.ca",
    "peertube.genma.fr",
    "peertube.get-racing.de",
    "peertube.ghis94.ovh",
    "peertube.gidikroon.eu",
    "peertube.giftedmc.com",
    "peertube.gravitywell.xyz",
    "peertube.grosist.fr",
    "peertube.h-u.social",
    "peertube.habets.house",
    "peertube.hackerfoo.com",
    "peertube.havesexwith.men",
    "peertube.hellsite.net",
    "peertube.helvetet.eu",
    "peertube.heraut.eu",
    "peertube.histoirescrepues.fr",
    "peertube.hizkia.eu",
    "peertube.home.x0r.fr",
    "peertube.hosnet.fr",
    "peertube.hyperfreedom.org",
    "peertube.ichigo.everydayimshuflin.com",
    "peertube.ignifi.me",
    "peertube.in.ua",
    "peertube.interhop.org",
    "peertube.intrapology.com",
    "peertube.inubo.ch",
    "peertube.iriseden.eu",
    "peertube.it-arts.net",
    "peertube.iz5wga.radio",
    "peertube.jackbot.fr",
    "peertube.jarmvl.net",
    "peertube.jussak.net",
    "peertube.kaleidos.net",
    "peertube.kalua.im",
    "peertube.kameha.click",
    "peertube.keazilla.net",
    "peertube.klaewyss.fr",
    "peertube.kleph.eu",
    "peertube.kriom.net",
    "peertube.kx.studio",
    "peertube.kyriog.eu",
    "peertube.laas.fr",
    "peertube.labeuropereunion.eu",
    "peertube.lagob.fr",
    "peertube.lagvoid.com",
    "peertube.laveinal.cat",
    "peertube.lesparasites.net",
    "peertube.lhc.lu",
    "peertube.lhc.net.br",
    "peertube.libresolutions.network",
    "peertube.libretic.fr",
    "peertube.linsurgee.fr",
    "peertube.livingutopia.org",
    "peertube.local.tilera.xyz",
    "peertube.logilab.fr",
    "peertube.louisematic.site",
    "peertube.luckow.org",
    "peertube.luga.at",
    "peertube.lyceeconnecte.fr",
    "peertube.lyclpg.itereva.pf",
    "peertube.magicstone.dev",
    "peertube.makotoworkshop.org",
    "peertube.manalejandro.com",
    "peertube.marcelsite.com",
    "peertube.marienschule.de",
    "peertube.martiabernathey.com",
    "peertube.marud.fr",
    "peertube.meditationsteps.org",
    "peertube.mesnumeriques.fr",
    "peertube.metalbanana.net",
    "peertube.mikemestnik.net",
    "peertube.minetestserver.ru",
    "peertube.modspil.dk",
    "peertube.monicz.dev",
    "peertube.monlycee.net",
    "peertube.musicstudio.pro",
    "peertube.mygaia.org",
    "peertube.nadeko.net",
    "peertube.nashitut.ru",
    "peertube.nayya.org",
    "peertube.netzbegruenung.de",
    "peertube.nicolastissot.fr",
    "peertube.nighty.name",
    "peertube.nissesdomain.org",
    "peertube.node14del46.gadix.net",
    "peertube.nodja.com",
    "peertube.nogafam.fr",
    "peertube.nomagic.uk",
    "peertube.normalgamingcommunity.cz",
    "peertube.ntc.dsausa.org",
    "peertube.nuage-libre.fr",
    "peertube.nz",
    "peertube.offerman.com",
    "peertube.ohioskates.com",
    "peertube.opencloud.lu",
    "peertube.openstreetmap.fr",
    "peertube.otakufarms.com",
    "peertube.pablopernot.fr",
    "peertube.paladyn.org",
    "peertube.parenti.net",
    "peertube.pcservice46.fr",
    "peertube.pix-n-chill.fr",
    "peertube.plataformess.org",
    "peertube.plaureano.nohost.me",
    "peertube.pogmom.me",
    "peertube.protagio.org",
    "peertube.public.cat",
    "peertube.qontinuum.space",
    "peertube.qtg.fr",
    "peertube.r2.enst.fr",
    "peertube.r5c3.fr",
    "peertube.ra.no",
    "peertube.rainbowswingers.net",
    "peertube.redpill-insight.com",
    "peertube.researchinstitute.at",
    "peertube.rhoving.com",
    "peertube.rlp.schule",
    "peertube.roflcopter.fr",
    "peertube.rokugan.fr",
    "peertube.rougevertbleu.tv",
    "peertube.roundpond.net",
    "peertube.rural-it.org",
    "peertube.s2s.video",
    "peertube.satoshishop.de",
    "peertube.securitymadein.lu",
    "peertube.semperpax.com",
    "peertube.semweb.pro",
    "peertube.sensin.eu",
    "peertube.shilohnewark.org",
    "peertube.simounet.net",
    "peertube.sjml.de",
    "peertube.skorpil.cz",
    "peertube.slat.org",
    "peertube.solidev.net",
    "peertube.stattzeitung.org",
    "peertube.stream",
    "peertube.swarm.solvingmaz.es",
    "peertube.swrs.net",
    "peertube.takeko.cyou",
    "peertube.tangentfox.com",
    "peertube.tata.casa",
    "peertube.teftera.com",
    "peertube.terranout.mine.nu",
    "peertube.th3rdsergeevich.xyz",
    "peertube.ti-fr.com",
    "peertube.tiennot.net",
    "peertube.tmp.rcp.tf",
    "peertube.touhoppai.moe",
    "peertube.travelpandas.eu",
    "peertube.troback.com",
    "peertube.tspu.edu.ru",
    "peertube.tspu.ru",
    "peertube.tv",
    "peertube.tweb.tv",
    "peertube.ucy.de",
    "peertube.universiteruraledescevennes.org",
    "peertube.unixweb.net",
    "peertube.uno",
    "peertube.vapronva.pw",
    "peertube.veen.world",
    "peertube.vesdia.eu",
    "peertube.vhack.eu",
    "peertube.virtual-assembly.org",
    "peertube.viviers-fibre.net",
    "peertube.vlaki.cz",
    "peertube.waldstepperbu.de",
    "peertube.we-keys.fr",
    "peertube.winscloud.net",
    "peertube.wirenboard.com",
    "peertube.wtf",
    "peertube.wtfayla.net",
    "peertube.xn--gribschi-o4a.ch",
    "peertube.xrcb.cat",
    "peertube.xwiki.com",
    "peertube.yujiri.xyz",
    "peertube.zalasur.media",
    "peertube.zergy.net",
    "peertube.zmuuf.org",
    "peertube.zvcdn.de",
    "peertube.zwindler.fr",
    "peertube2.cpy.re",
    "peertube3.cpy.re",
    "peertube400.pocketnet.app",
    "peertube600.pocketnet.app",
    "peertubecz.duckdns.org",
    "peertubevdb.de",
    "peervideo.club",
    "peervideo.ru",
    "periscope.numenaute.org",
    "pete.warpnine.de",
    "petitlutinartube.fr",
    "pfideo.pfriedma.org",
    "phijkchu.com",
    "phoenixproject.group",
    "piped.chrisco.me",
    "piraten.space",
    "pire.artisanlogiciel.net",
    "pirtube.calut.fr",
    "platt.video",
    "play-my.video",
    "play.dfri.se",
    "play.kryta.app",
    "play.mittdata.se",
    "play.rejas.se",
    "play.shirtless.gay",
    "player.ojamajo.moe",
    "po0.online",
    "podlibre.video",
    "pony.tube",
    "portal.digilab.nfa.cz",
    "praxis.su",
    "praxis.tube",
    "private.fedimovie.com",
    "prtb.komaniya.work",
    "pt.freedomwolf.cc",
    "pt.gordons.gen.nz",
    "pt.hexor.cy",
    "pt.ilyamikcoder.com",
    "pt.mezzo.moe",
    "pt.minhinprom.ru",
    "pt.na4.eu",
    "pt.netcraft.ch",
    "pt.nijbakker.net",
    "pt.rwx.ch",
    "pt.sarahgebauer.com",
    "pt.scrunkly.cat",
    "pt.sfunk1x.com",
    "pt.thishorsie.rocks",
    "pt.twinkle.lol",
    "pt.vern.cc",
    "pt01.lehrerfortbildung-bw.de",
    "ptube.rousset.nom.fr",
    "publicvideo.nl",
    "punktube.net",
    "puppet.zone",
    "puptube.rodeo",
    "qtube.qlyoung.net",
    "quantube.win",
    "quebec1.freediverse.com",
    "rankett.net",
    "raptube.antipub.org",
    "refuznik.video",
    "regarder.sans.pub",
    "replay.jres.org",
    "retvrn.tv",
    "sc07.tv",
    "scitech.video",
    "sdmtube.fr",
    "see.ellipsenpark.de",
    "seka.pona.la",
    "sermons.luctorcrc.org",
    "serv3.wiki-tube.de",
    "sfba.video",
    "share.tube",
    "sizetube.com",
    "skeptikon.fr",
    "skeptube.fr",
    "social.fedimovie.com",
    "sovran.video",
    "special.videovortex.tv",
    "spectra.video",
    "spook.tube",
    "stl1988.peertube-host.de",
    "stream.andersonr.net",
    "stream.biovisata.lt",
    "stream.conesphere.cloud",
    "stream.elven.pw",
    "stream.jurnalfm.md",
    "stream.k-prod.fr",
    "stream.litera.tools",
    "stream.nuemedia.se",
    "stream.rlp-media.de",
    "stream.udk-berlin.de",
    "stream.vrse.be",
    "streamarchive.manicphase.me",
    "studio.lrnz.it",
    "studios.racer159.com",
    "study.miet.ru",
    "styxhexenhammer666.com",
    "subscribeto.me",
    "syrteplay.obspm.fr",
    "t.0x0.st",
    "tbh.co-shaoghal.net",
    "telegenic.talesofmy.life",
    "testube.distrilab.fr",
    "theater.ethernia.net",
    "thecool.tube",
    "tilvids.com",
    "tinkerbetter.tube",
    "tinsley.video",
    "toobnix.org",
    "trailers.ddigest.com",
    "troll.tv",
    "tube-action-educative.apps.education.fr",
    "tube-arts-lettres-sciences-humaines.apps.education.fr",
    "tube-cycle-2.apps.education.fr",
    "tube-cycle-3.apps.education.fr",
    "tube-education-physique-et-sportive.apps.education.fr",
    "tube-enseignement-professionnel.apps.education.fr",
    "tube-institutionnel.apps.education.fr",
    "tube-langues-vivantes.apps.education.fr",
    "tube-maternelle.apps.education.fr",
    "tube-numerique-educatif.apps.education.fr",
    "tube-sciences-technologies.apps.education.fr",
    "tube-test.apps.education.fr",
    "tube.2hyze.de",
    "tube.4e6a.ru",
    "tube.aetherial.xyz",
    "tube.alado.space",
    "tube.alphonso.fr",
    "tube.anjara.eu",
    "tube.anufrij.de",
    "tube.aquilenet.fr",
    "tube.ar.hn",
    "tube.archworks.co",
    "tube.area404.cloud",
    "tube.arthack.nz",
    "tube.artvage.com",
    "tube.asulia.fr",
    "tube.azbyka.ru",
    "tube.balamb.fr",
    "tube.beit.hinrichs.cc",
    "tube.bigpicture.watch",
    "tube.bit-friends.de",
    "tube.bsd.cafe",
    "tube.bstly.de",
    "tube.buchstoa-tv.at",
    "tube.calculate.social",
    "tube.chach.org",
    "tube.chaoszone.tv",
    "tube.chaun14.fr",
    "tube.chispa.fr",
    "tube.cms.garden",
    "tube.communia.org",
    "tube.crapaud-fou.org",
    "tube.croustifed.net",
    "tube.cyano.at",
    "tube.cybertopia.xyz",
    "tube.dembased.xyz",
    "tube.dev.displ.eu",
    "tube.die-rote-front.de",
    "tube.distrilab.fr",
    "tube.dnet.one",
    "tube.doortofreedom.org",
    "tube.dsocialize.net",
    "tube.ebin.club",
    "tube.elemac.fr",
    "tube.erzbistum-hamburg.de",
    "tube.extinctionrebellion.fr",
    "tube.fdn.fr",
    "tube.fede.re",
    "tube.fedisphere.net",
    "tube.fediverse.at",
    "tube.felinn.org",
    "tube.flokinet.is",
    "tube.freeit247.eu",
    "tube.friloux.me",
    "tube.froth.zone",
    "tube.fulda.social",
    "tube.funil.de",
    "tube.futuretic.fr",
    "tube.g1sms.fr",
    "tube.g4rf.net",
    "tube.gaiac.io",
    "tube.garrett.life",
    "tube.gayfr.online",
    "tube.geekyboo.net",
    "tube.genb.de",
    "tube.ghk-academy.info",
    "tube.gi-it.de",
    "tube.govital.net",
    "tube.grap.coop",
    "tube.graz.social",
    "tube.grin.hu",
    "tube.gummientenmann.de",
    "tube.hamakor.org.il",
    "tube.hapyyr.com",
    "tube.helpsolve.org",
    "tube.hoga.fr",
    "tube.homecomputing.fr",
    "tube.infrarotmedien.de",
    "tube.int5.net",
    "tube.interhacker.space",
    "tube.io18.top",
    "tube.jeena.net",
    "tube.jesusgomez.me",
    "tube.kdy.ch",
    "tube.kh-berlin.de",
    "tube.kher.nl",
    "tube.kicou.info",
    "tube.kockatoo.org",
    "tube.kotocoop.org",
    "tube.kotur.org",
    "tube.koweb.fr",
    "tube.lab.nrw",
    "tube.lacaveatonton.ovh",
    "tube.laurent-malys.fr",
    "tube.le-gurk.de",
    "tube.leetdreams.ch",
    "tube.linkse.media",
    "tube.lokad.com",
    "tube.lubakiagenda.net",
    "tube.lucie-philou.com",
    "tube.matrix.rocks",
    "tube.me.jon-e.net",
    "tube.mediasculp.com",
    "tube.mfraters.net",
    "tube.moep.tv",
    "tube.morozoff.pro",
    "tube.nestor.coop",
    "tube.netz-knoten.de",
    "tube.niel.me",
    "tube.nieuwwestbrabant.nl",
    "tube.nogafa.org",
    "tube.nox-rhea.org",
    "tube.nuagelibre.fr",
    "tube.numerique.gouv.fr",
    "tube.nuxnik.com",
    "tube.nx-pod.de",
    "tube.nx12.net",
    "tube.oisux.org",
    "tube.onlinekirche.net",
    "tube.opportunis.me",
    "tube.org.il",
    "tube.otter.sh",
    "tube.p2p.legal",
    "tube.pari.cafe",
    "tube.parinux.org",
    "tube.pastwind.top",
    "tube.picasoft.net",
    "tube.pifferi.io",
    "tube.pilgerweg-21.de",
    "tube.plaf.fr",
    "tube.pmj.rocks",
    "tube.pol.social",
    "tube.polytech-reseau.org",
    "tube.pompat.us",
    "tube.ponsonaille.fr",
    "tube.portes-imaginaire.org",
    "tube.postblue.info",
    "tube.purser.it",
    "tube.pustule.org",
    "tube.raccoon.quest",
    "tube.rebellion.global",
    "tube.reseau-canope.fr",
    "tube.rfc1149.net",
    "tube.rhythms-of-resistance.org",
    "tube.risedsky.ovh",
    "tube.rooty.fr",
    "tube.rsi.cnr.it",
    "tube.ryne.moe",
    "tube.sadlads.com",
    "tube.sasek.tv",
    "tube.sbcloud.cc",
    "tube.schleuss.online",
    "tube.schule.social",
    "tube.sector1.fr",
    "tube.sekretaerbaer.net",
    "tube.shanti.cafe",
    "tube.shela.nu",
    "tube.skrep.in",
    "tube.sloth.network",
    "tube.sp-codes.de",
    "tube.spdns.org",
    "tube.ssh.club",
    "tube.swee.codes",
    "tube.systemz.pl",
    "tube.systerserver.net",
    "tube.taker.fr",
    "tube.tchncs.de",
    "tube.techeasy.org",
    "tube.teckids.org",
    "tube.thechangebook.org",
    "tube.theliberatededge.org",
    "tube.tilera.xyz",
    "tube.tinfoil-hat.net",
    "tube.tkzi.ru",
    "tube.todon.eu",
    "tube.toldi.eu",
    "tube.transgirl.fr",
    "tube.trax.im",
    "tube.ttk.is",
    "tube.tuxfriend.fr",
    "tube.tylerdavis.xyz",
    "tube.ukcolumn.org",
    "tube.uncomfortable.business",
    "tube.undernet.uy",
    "tube.unif.app",
    "tube.vencabot.com",
    "tube.vrpnet.org",
    "tube.waag.org",
    "tube.whytheyfight.com",
    "tube.xd0.de",
    "tube.xn--baw-joa.social",
    "tube.xy-space.de",
    "tube.yapbreak.fr",
    "tubedu.org",
    "tubocatodico.bida.im",
    "tubulus.openlatin.org",
    "tueb.telent.net",
    "tututu.tube",
    "tuvideo.encanarias.info",
    "tux-edu.tv",
    "tv.adast.dk",
    "tv.adn.life",
    "tv.anarchy.bg",
    "tv.animalcracker.art",
    "tv.arns.lt",
    "tv.atmx.ca",
    "tv.cuates.net",
    "tv.dyne.org",
    "tv.farewellutopia.com",
    "tv.filmfreedom.net",
    "tv.gravitons.org",
    "tv.kobold-cave.eu",
    "tv.lumbung.space",
    "tv.nizika.tv",
    "tv.pirateradio.social",
    "tv.pirati.cz",
    "tv.santic-zombie.ru",
    "tv.speleo.mooo.com",
    "tv.undersco.re",
    "tv.zonepl.net",
    "tvox.ru",
    "twctube.twc-zone.eu",
    "urbanists.video",
    "user800.one",
    "v.basspistol.org",
    "v.blustery.day",
    "v.comfycamp.space",
    "v.esd.cc",
    "v.eurorede.com",
    "v.kisombrella.top",
    "v.kretschmann.social",
    "v.lor.sh",
    "v.mbius.io",
    "v.mkp.ca",
    "v.pizda.world",
    "v.posm.gay",
    "v0.trm.md",
    "v1.smartit.nu",
    "vamzdis.group.lt",
    "varis.tv",
    "vdo.greboca.com",
    "vdo.unvanquished.greboca.com",
    "veedeo.org",
    "vhs.absturztau.be",
    "vhsky.cz",
    "vibeos.grampajoe.online",
    "vid.cthos.dev",
    "vid.digitaldragon.club",
    "vid.fossdle.org",
    "vid.freedif.org",
    "vid.jittr.click",
    "vid.kinuseka.us",
    "vid.mawuki.de",
    "vid.mkp.ca",
    "vid.nocogabriel.fr",
    "vid.norbipeti.eu",
    "vid.northbound.online",
    "vid.nsf-home.ip-dynamic.org",
    "vid.ohboii.de",
    "vid.plantplotting.co.uk",
    "vid.pretok.tv",
    "vid.prometheus.systems",
    "vid.ryg.one",
    "vid.twhtv.club",
    "vid.wildeboer.net",
    "vid.y-y.li",
    "videa.inspirujici.cz",
    "video-cave-v2.de",
    "video.076.moe",
    "video.076.ne.jp",
    "video.1146.nohost.me",
    "video.383.su",
    "video.3cmr.fr",
    "video.4d2.org",
    "video.9wd.eu",
    "video.abraum.de",
    "video.adamwilbert.com",
    "video.administrieren.net",
    "video.ados.accoord.fr",
    "video.akk.moe",
    "video.altertek.org",
    "video.alton.cloud",
    "video.anaproy.nl",
    "video.anartist.org",
    "video.angrynerdspodcast.nl",
    "video.antopie.org",
    "video.apz.fi",
    "video.arghacademy.org",
    "video.asgardius.company",
    "video.asturias.red",
    "video.audiovisuel-participatif.org",
    "video.balfolk.social",
    "video.barcelo.ynh.fr",
    "video.bards.online",
    "video.beartrix.au",
    "video.benetou.fr",
    "video.beyondwatts.social",
    "video.bgeneric.net",
    "video.bilecik.edu.tr",
    "video.birkeundnymphe.de",
    "video.blast-info.fr",
    "video.blazma.st",
    "video.bmu.cloud",
    "video.boxingpreacher.net",
    "video.brothertec.eu",
    "video.canadiancivil.com",
    "video.cartoon-aa.xyz",
    "video.catgirl.biz",
    "video.cats-home.net",
    "video.causa-arcana.com",
    "video.chalec.org",
    "video.chasmcity.net",
    "video.chbmeyer.de",
    "video.chipio.industries",
    "video.cigliola.com",
    "video.citizen4.eu",
    "video.cm-en-transition.fr",
    "video.cnnumerique.fr",
    "video.cnr.it",
    "video.coales.co",
    "video.codefor.de",
    "video.coffeebean.social",
    "video.colibris-outilslibres.org",
    "video.collectifpinceoreilles.com",
    "video.colmaris.fr",
    "video.comune.trento.it",
    "video.consultatron.com",
    "video.coop.tools",
    "video.coyp.us",
    "video.cpn.so",
    "video.cybersystems.engineer",
    "video.davduf.net",
    "video.davejansen.com",
    "video.dhamdomum.ynh.fr",
    "video.dlearning.nl",
    "video.dnfi.no",
    "video.dresden.network",
    "video.echelon.pl",
    "video.echirolles.fr",
    "video.edu.nl",
    "video.eientei.org",
    "video.elfhosted.com",
    "video.ellijaymakerspace.org",
    "video.emergeheart.info",
    "video.espr.moe",
    "video.europalestine.com",
    "video.expiredpopsicle.com",
    "video.extremelycorporate.ca",
    "video.fabiomanganiello.com",
    "video.fabriquedelatransition.fr",
    "video.fdlibre.eu",
    "video.fedi.bzh",
    "video.fedihost.co",
    "video.fhtagn.org",
    "video.firehawk-systems.com",
    "video.firesidefedi.live",
    "video.fission.social",
    "video.floor9.com",
    "video.fnordkollektiv.de",
    "video.foofus.com",
    "video.fox-romka.ru",
    "video.franzgraf.de",
    "video.fuss.bz.it",
    "video.g3l.org",
    "video.gamerstavern.online",
    "video.gemeinde-pflanzen.net",
    "video.graine-pdl.org",
    "video.gresille.org",
    "video.gyt.is",
    "video.hacklab.fi",
    "video.hainry.fr",
    "video.hardlimit.com",
    "video.hdys.band",
    "video.icic.law",
    "video.igem.org",
    "video.infiniteloop.tv",
    "video.infojournal.fr",
    "video.infosec.exchange",
    "video.innovationhub-act.org",
    "video.internet-czas-dzialac.pl",
    "video.iphodase.fr",
    "video.ipng.ch",
    "video.irem.univ-paris-diderot.fr",
    "video.ironsysadmin.com",
    "video.jacen.moe",
    "video.jadin.me",
    "video.jeffmcbride.net",
    "video.jigmedatse.com",
    "video.katehildenbrand.com",
    "video.kompektiva.org",
    "video.kuba-orlik.name",
    "video.lacalligramme.fr",
    "video.lala.ovh",
    "video.lamer-ethos.site",
    "video.lanceurs-alerte.fr",
    "video.laotra.red",
    "video.lapineige.fr",
    "video.laraffinerie.re",
    "video.latavernedejohnjohn.fr",
    "video.lavolte.net",
    "video.lemediatv.fr",
    "video.liberta.vip",
    "video.libreti.net",
    "video.linc.systems",
    "video.linux.it",
    "video.linuxtrent.it",
    "video.livecchi.cloud",
    "video.liveitlive.show",
    "video.lmika.org",
    "video.lono.space",
    "video.lqdn.fr",
    "video.lunago.net",
    "video.lundi.am",
    "video.lw1.at",
    "video.lycee-experimental.org",
    "video.maechler.cloud",
    "video.magical.fish",
    "video.magikh.fr",
    "video.manicphase.me",
    "video.marcorennmaus.de",
    "video.matomocamp.org",
    "video.medienzentrum-harburg.de",
    "video.mentality.rip",
    "video.metaccount.de",
    "video.mikepj.dev",
    "video.millironx.com",
    "video.mobile-adenum.fr",
    "video.mondoweiss.net",
    "video.monsieurbidouille.fr",
    "video.mshparisnord.fr",
    "video.mttv.it",
    "video.mugoreve.fr",
    "video.mxtthxw.art",
    "video.mycrowd.ca",
    "video.na-prostem.si",
    "video.nesven.eu",
    "video.netsyms.com",
    "video.ngi.eu",
    "video.niboe.info",
    "video.nluug.nl",
    "video.nogafam.es",
    "video.nstr.no",
    "video.nuage-libre.fr",
    "video.off-investigation.fr",
    "video.oh14.de",
    "video.olisti.co",
    "video.olos311.org",
    "video.omada.cafe",
    "video.omniatv.com",
    "video.onjase.quebec",
    "video.osgeo.org",
    "video.ourcommon.cloud",
    "video.ozgurkon.org",
    "video.passageenseine.fr",
    "video.patiosocial.es",
    "video.pavel-english.ru",
    "video.pcf.fr",
    "video.pcgaldo.com",
    "video.pizza.enby.city",
    "video.pizza.ynh.fr",
    "video.ploss-ra.fr",
    "video.ploud.jp",
    "video.poul.org",
    "video.procolix.eu",
    "video.pronkiewicz.pl",
    "video.publicspaces.net",
    "video.pullopen.xyz",
    "video.r3s.nrw",
    "video.rastapuls.com",
    "video.resolutions.it",
    "video.retroedge.tech",
    "video.rhizome.org",
    "video.riquy.dev",
    "video.rlp-media.de",
    "video.rs-einrich.de",
    "video.rubdos.be",
    "video.sadmin.io",
    "video.sadrarin.com",
    "video.selea.se",
    "video.sidh.bzh",
    "video.silex.me",
    "video.simplex-software.ru",
    "video.snug.moe",
    "video.software-fuer-engagierte.de",
    "video.sorokin.music",
    "video.starysacz.um.gov.pl",
    "video.stuve-bamberg.de",
    "video.taboulisme.com",
    "video.taskcards.eu",
    "video.team-lcbs.eu",
    "video.tedomum.net",
    "video.telemillevaches.net",
    "video.thepolarbear.co.uk",
    "video.thinkof.name",
    "video.thomaspreece.net",
    "video.thoshis.net",
    "video.tkz.es",
    "video.triplea.fr",
    "video.tryptophonic.com",
    "video.turbo-kermis.fr",
    "video.turbo.chat",
    "video.typica.us",
    "video.uriopss-pdl.fr",
    "video.ut0pia.org",
    "video.vaku.org.ua",
    "video.valme.io",
    "video.veen.world",
    "video.vegafjord.me",
    "video.violoncello.ch",
    "video.voiceover.bar",
    "video.windfluechter.org",
    "video.worteks.com",
    "video.writeas.org",
    "video.wszystkoconajwazniejsze.pl",
    "video.xmpp-it.net",
    "video.xorp.hu",
    "video.ziez.eu",
    "video.zlinux.ru",
    "video.zonawarpa.it",
    "video2.echelon.pl",
    "videohaven.com",
    "videos-libr.es",
    "videos-passages.huma-num.fr",
    "videos.80px.com",
    "videos.aadtp.be",
    "videos.abnormalbeings.space",
    "videos.adhocmusic.com",
    "videos.ahp-numerique.fr",
    "videos.alamaisondulibre.org",
    "videos.ananace.dev",
    "videos.archigny.net",
    "videos.ardmoreleader.com",
    "videos.avency.de",
    "videos.b4tech.org",
    "videos.bik.opencloud.lu",
    "videos.brookslawson.com",
    "videos.capitoledulibre.org",
    "videos.cemea.org",
    "videos.chardonsbleus.org",
    "videos.cloudron.io",
    "videos.codingotaku.com",
    "videos.coletivos.org",
    "videos.conferences-gesticulees.net",
    "videos.danksquad.org",
    "videos.devteams.at",
    "videos.domainepublic.net",
    "videos.draculo.net",
    "videos.dromeadhere.fr",
    "videos.enisa.europa.eu",
    "videos.erg.be",
    "videos.espitallier.net",
    "videos.explain-it.org",
    "videos.foilen.com",
    "videos.foilen.net",
    "videos.fozfuncs.com",
    "videos.fsci.in",
    "videos.gamercast.net",
    "videos.gianmarco.gg",
    "videos.gl75.fr",
    "videos.globenet.org",
    "videos.grafo.zone",
    "videos.hack2g2.fr",
    "videos.hardcoredevs.com",
    "videos.hauspie.fr",
    "videos.icum.to",
    "videos.idiocy.xyz",
    "videos.ikacode.com",
    "videos.im.allmendenetz.de",
    "videos.imjessewellman.blog",
    "videos.irrelevant.me.uk",
    "videos.jacksonchen666.com",
    "videos.jevalide.ca",
    "videos.john-livingston.fr",
    "videos.knazarov.com",
    "videos.koumoul.com",
    "videos.kuoushi.com",
    "videos.laliguepaysdelaloire.org",
    "videos.lemnoslife.com",
    "videos.lemouvementassociatif-pdl.org",
    "videos.lescommuns.org",
    "videos.leslionsfloorball.fr",
    "videos.libervia.org",
    "videos.librescrum.org",
    "videos.martyn.berlin",
    "videos.miolo.org",
    "videos.monstro1.com",
    "videos.mykdeen.com",
    "videos.nerdout.online",
    "videos.netwaver.xyz",
    "videos.noeontheend.com",
    "videos.npo.city",
    "videos.nuculabs.dev",
    "videos.offroad.town",
    "videos.pair2jeux.tube",
    "videos.parleur.net",
    "videos.phegan.live",
    "videos.pixelpost.uk",
    "videos.pkutalk.com",
    "videos.rampin.org",
    "videos.rauten.co.za",
    "videos.ritimo.org",
    "videos.scanlines.xyz",
    "videos.shendrick.net",
    "videos.shmalls.pw",
    "videos.side-ways.net",
    "videos.spacebar.ca",
    "videos.stadtfabrikanten.org",
    "videos.supertuxkart.net",
    "videos.tcit.fr",
    "videos.testimonia.org",
    "videos.tfcconnection.org",
    "videos.thinkerview.com",
    "videos.tiffanysostar.com",
    "videos.triceraprog.fr",
    "videos.trom.tf",
    "videos.utsukta.org",
    "videos.viorsan.com",
    "videos.wikilibriste.fr",
    "videos.yesil.club",
    "videos.yeswiki.net",
    "videoteca.kenobit.it",
    "videotube.duckdns.org",
    "videovortex.tv",
    "videowisent.maw.best",
    "vids.grueproof.net",
    "vids.krserv.social",
    "vids.roshless.me",
    "vids.stary.pc.pl",
    "vids.tekdmn.me",
    "vids.ttlmakerspace.com",
    "vidz.dou.bet",
    "vidz.julien.ovh",
    "views.southfox.me",
    "virtual-girls-are.definitely-for.me",
    "viste.pt",
    "vn.jvideos.top",
    "vod.ksite.de",
    "vod.newellijay.tv",
    "vods.juni.tube",
    "voluntarytube.com",
    "vtr.chikichiki.tube",
    "vulgarisation-informatique.fr",
    "wacha.punks.cc",
    "watch.easya.solutions",
    "watch.eeg.cl.cam.ac.uk",
    "watch.goodluckgabe.life",
    "watch.jimmydore.com",
    "watch.libertaria.space",
    "watch.ocaml.org",
    "watch.oroykhon.ru",
    "watch.softinio.com",
    "watch.tacticaltech.org",
    "watch.thelema.social",
    "watch.weanimatethings.com",
    "we.haydn.rocks",
    "weare.dcnh.tv",
    "webtv.vandoeuvre.net",
    "wiwi.video",
    "woodland.video",
    "wtfayla.com",
    "wur.pm",
    "www.earthclimate.tv",
    "www.kotikoff.net",
    "www.makertube.net",
    "www.mypeer.tube",
    "www.nadajemy.com",
    "www.neptube.io",
    "www.piratentube.de",
    "www.rocaguinarda.tv",
    "www.vev.su",
    "www.videos-libr.es",
    "www.yiny.org",
    "www.zohup.link",
    "xn--fsein-zqa5f.xn--nead-na-bhfinleog-hpb.ie",
    "xxivproduction.video",
    "ysm.info",
    "yt.antebeot.world",
    "yt.lostpod.space",
    "yt.novelity.tech",
    "yt.orokoro.ru",
    "ytube.retronerd.at",
    "yuitobe.wikiwiki.li",
    "yunopeertube.myddns.me",
}

__all__ = ['instances']
